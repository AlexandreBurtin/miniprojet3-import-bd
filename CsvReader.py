import sqlite3
import csv
import logging
#lecture d'un fichier CSV passé en paramètre
def lecture_csv(filepath,delimiter=';'):
    logging.info("Reading file...")
    dataList=[]
    with open(filepath,'r') as csvin:
        csvreader=csv.DictReader(csvin,delimiter=delimiter)
        for data in csvreader:
            dataList.append(data)
    logging.info("Read complete")
    return dataList