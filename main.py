import sqlite3
import sys
from CsvReader import lecture_csv
from VerificationBdd import verification_Database,verification_table
from Bddcommand import connectionbdd,create_table,insertion_Bdd
import logging
#Crée une une base/table si elle n'existe pas et insère des données a partir du fichier CSv
def main():
    logging.basicConfig(filename="Log.info",level=logging.DEBUG)
    logging.info("Start")
    Data=lecture_csv('auto.csv')
    if verification_Database()==False:
        logging.info("Database doesn't exist")
        login=connectionbdd()
        cursor=login.cursor()
        create_table(login,cursor)
        insertion_Bdd(login,cursor,Data)
    else:
        logging.info("Database Already exist")
        login=connectionbdd()
        cursor=login.cursor()
        if verification_table(login,cursor)==True:
            logging.info("Table Already exist")
            insertion_Bdd(login,cursor,Data)
        else:
            logging.info("Table doesn't exist")
            create_table(login,cursor)
            insertion_Bdd(login,cursor,Data)
    logging.info("Finish")
if __name__=="__main__":
    main()
