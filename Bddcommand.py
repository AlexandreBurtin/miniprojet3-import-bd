import sqlite3
import logging

#Connection à une base de données 
def connectionbdd():
    connection=sqlite3.connect("Bdd.sqlite3")
    logging.info("Connected to Bdd.sqlite3")
    return connection
#Créer une table via le fichier CreateTable.sql si elle n'existe pas 
def create_table(login,cursor):
    logging.info("Reading file....")
    with open('CreateTable.sql','r') as sql_file:
            sql_script=sql_file.read()
    logging.info("Read finish")
    cursor.executescript(sql_script)
    logging.info("Table create")
    login.commit()

#Insere une base de données fourni en paramètre dans une base de données 
def insertion_Bdd(login,cursor,Data):
    for i in range(len(Data)):
        liste=[]
        for key in Data[i]:
            liste.append(Data[i][key])
        values=(liste[3],)
        cursor.execute("SELECT immatriculation FROM Voiture WHERE immatriculation=?",values)
        string=cursor.fetchone()
        if string==values:
            logging.info("immatriculation Already exist")
            cursor.execute("DELETE FROM Voiture WHERE immatriculation=(?)",values)
            logging.info("Deleting data...")
        cursor.execute("INSERT INTO Voiture VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",liste)
        logging.info("Insertion complete")
        login.commit()
    login.close()
    logging.info("Close DataBase")
    