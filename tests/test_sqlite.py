import unittest
import sys
sys.path[:0]=["../"]
from CsvReader import lecture_csv
from BddCommand import insertion_Bdd
import sqlite3
import os

class TestCsvReader(unittest.TestCase):
    def setUp(self):
        self.connection=sqlite3.connect('tests/testbdd.sqlite3')
        self.cursor=self.connection.cursor()
        with open('test/TestCreateTable.sql','r') as sql_file:
            sql_script=sql_file.read()
        self.cursor.executescript(sql_script)
        self.connection.commit()
        self.connection.commit()
    def tearDown(self):
        self.cursor.execute('''DROP TABLE  Auto''')
        self.connection.commit()
        self.connection.close()
        os.remove("tests/testbdd.sqlite3")
    def test_upsert(self):
        self.cursor.execute("SELECT COUNT(*) FROM test")
        string=self.cursor.fetchall()
        self.assertEqual(string[0][0],0)
        valuelist={'adresse_titulaire': '3822 Omar Square Suite 257 Port Emily, OK 43251', 'carrosserie': '45-1743376',
        'categorie': '34-7904216', 'couleur': 'LightGoldenRodYellow', 'cylindre': '3462', 'date_immatriculation': '03/05/2012',
        'denomination_commerciale': 'Enhanced well-modul...moderator', 'energie': '37578077', 'immatriculation': 'OVC-568',
        'marque': 'Williams Inc', 'nom': 'Smith', 'places': '32', 'poids': '3827', 'prenom': 'Jerome','immatriculation':'OVC-568',
        'date_immatriculation': '03/05/2012',}
        insertion_Bdd(self.connection,self.cursor,valueList)
        self.cursor.execute("SELECT * FROM Auto")
        string=self.cursor.fetchall()
        self.assertEqual(string,'1 2')